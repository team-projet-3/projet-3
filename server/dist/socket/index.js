"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var socket = require("socket.io");
var chat_socket_handler_1 = require("./handlers/chat.socket.handler");
var socket_1 = require("./core/socket");
var groups_socket_handler_1 = require("./handlers/groups.socket.handler");
var game_socket_handler_1 = require("./handlers/game.socket.handler");
var game_chat_socket_handler_1 = require("./handlers/game.chat.socket.handler");
var virtual_player_1 = require("./games/virtual.player");
var auth_server_service_1 = require("../services/auth.server.service");
var fs = require("fs");
var Socket = /** @class */ (function () {
    function Socket(http) {
        var _this = this;
        this.io = socket(http);
        this.userSocketMap = {};
        this.gameChat = new game_chat_socket_handler_1.GameChatSocketHandler();
        this.chatHandler = new chat_socket_handler_1.ChatSocketHandler(this.gameChat);
        this.gameHandler = new game_socket_handler_1.GameSocketHandler(this.gameChat);
        this.groupHandler = new groups_socket_handler_1.GroupsSocketHandler(this.gameHandler, this.gameChat);
        this.globalVirtualPlayer = new virtual_player_1.VirtualPlayer();
        this.users = JSON.parse(fs.readFileSync("users.json", "utf8"));
        this.users.forEach(function (u) { return _this.logoutUser(u); });
        this.connect();
    }
    Socket.prototype.connect = function () {
        var _this = this;
        this.io.on("connection", function (s) {
            console.log("connected : " + s.id);
            if (!_this.userSocketMap[s.id]) {
                _this.setUsername(s);
                _this.handlers(s, new socket_1.CustomSocket(s));
            }
            else {
                console.log("User already Connected");
            }
        });
    };
    Socket.prototype.setUsername = function (s) {
        var _this = this;
        this.userSocketMap[s.id] = s.handshake.query.username;
        if (this.userSocketMap[s.id])
            this.addUser(this.userSocketMap[s.id]);
        s.on("username", function (username) {
            _this.userSocketMap[s.id] = username;
            _this.addUser(username);
        });
    };
    Socket.prototype.handlers = function (s, cs) {
        this.chatHandler.handle(cs);
        this.groupHandler.handle(cs);
        this.globalVirtualPlayer.handle(cs);
        this.disconnectHandler(s, cs);
    };
    Socket.prototype.disconnectHandler = function (s, cs) {
        var _this = this;
        s.on("disconnect", function () {
            _this.chatHandler.disconnect(cs);
            _this.groupHandler.disconnect(cs);
            _this.gameHandler.disconnect(cs);
            _this.logoutUser(_this.userSocketMap[s.id]);
            delete _this.userSocketMap[s.id];
            console.log("Socket disconnected : " + s.id);
        });
    };
    Socket.prototype.logoutUser = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("LOGGING OUT " + username);
                        return [4 /*yield*/, new auth_server_service_1.AuthService().logout(username)];
                    case 1:
                        _a.sent();
                        this.removeUser(username);
                        return [2 /*return*/];
                }
            });
        });
    };
    Socket.prototype.addUser = function (username) {
        this.users.push(username);
        fs.writeFileSync("users.json", JSON.stringify(this.users));
    };
    Socket.prototype.removeUser = function (username) {
        this.users = this.users.filter(function (u) { return u !== username; });
        fs.writeFileSync("users.json", JSON.stringify(this.users));
    };
    return Socket;
}());
exports.Socket = Socket;
//# sourceMappingURL=index.js.map