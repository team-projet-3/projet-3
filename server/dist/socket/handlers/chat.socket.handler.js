"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var events_1 = require("../core/events");
var chat_server_service_1 = require("../../services/chat.server.service");
var auth_server_service_1 = require("../../services/auth.server.service");
var ChatSocketHandler = /** @class */ (function () {
    function ChatSocketHandler(gameChat) {
        this.channels = [];
        this.chatService = new chat_server_service_1.ChatService();
        this.authService = new auth_server_service_1.AuthService();
        this.gameChat = gameChat;
    }
    ChatSocketHandler.prototype.handle = function (s) {
        var _this = this;
        s.on(events_1.Events.INIT_CHAT, function (username) {
            _this.initChat(username, s);
        });
        s.on(events_1.Events.ENTER_CHANNEL, function (it) {
            _this.enterChannel(it.channel, it.username, s);
        });
        s.on(events_1.Events.LEAVE_CHANNEL, function (it) {
            _this.leaveChannel(it.channel, it.username);
        });
        s.on(events_1.Events.NEW_MESSAGE, function (message) {
            if (message.channel === "game")
                _this.gameChat.message(s, message);
            else
                _this.newMessage(message);
        });
    };
    ChatSocketHandler.prototype.disconnect = function (s) {
        this.channels.forEach(function (c, i, a) {
            var idx = c.users.findIndex(function (user) { return user.s.id === s.id; });
            if (idx !== -1)
                a[i].users.splice(idx, 1);
        });
    };
    ChatSocketHandler.prototype.initChat = function (username, s) {
        return __awaiter(this, void 0, void 0, function () {
            var userStatus, channels, promises;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.authService.getUser(username)];
                    case 1:
                        userStatus = _a.sent();
                        if (!userStatus[0])
                            return [2 /*return*/];
                        channels = userStatus[1].channels;
                        promises = channels.map(function (channel) {
                            return _this.enterChannel(channel, username, s);
                        });
                        return [4 /*yield*/, Promise.all(promises)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ChatSocketHandler.prototype.enterChannel = function (name, username, s) {
        return __awaiter(this, void 0, void 0, function () {
            var channelStatus, channel;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("Enter Channel");
                        return [4 /*yield*/, this.chatService.getChannel(name)];
                    case 1:
                        channelStatus = _a.sent();
                        if (!channelStatus[0])
                            return [2 /*return*/];
                        channel = this.channels.find(function (c) { return c.name === name; });
                        if (!channel)
                            this.channels.push({ name: name, users: [{ username: username, s: s }] });
                        else if (channel.users.findIndex(function (u) { return u.username === username; }) === -1)
                            channel.users.push({ username: username, s: s });
                        console.log(username + " entered channel " + name);
                        return [2 /*return*/];
                }
            });
        });
    };
    ChatSocketHandler.prototype.leaveChannel = function (name, username) {
        return __awaiter(this, void 0, void 0, function () {
            var channel, idx;
            return __generator(this, function (_a) {
                channel = this.channels.find(function (c) { return c.name === name; });
                if (!channel)
                    return [2 /*return*/];
                idx = channel.users.findIndex(function (user) { return user.username === username; });
                if (idx === -1)
                    return [2 /*return*/];
                channel.users.splice(idx, 1);
                console.log(username + " left channel " + name);
                return [2 /*return*/];
            });
        });
    };
    ChatSocketHandler.prototype.newMessage = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var channel;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        channel = this.channels.find(function (c) { return c.name === message.channel; });
                        if (!channel)
                            return [2 /*return*/];
                        return [4 /*yield*/, this.chatService.addMessage(message)];
                    case 1:
                        if (!(_a.sent()))
                            return [2 /*return*/];
                        channel.users.forEach(function (user) { return user.s.emit(events_1.Events.NEW_MESSAGE, message); });
                        console.log(message.author + " sent a new message in channel " + message.channel);
                        return [2 /*return*/];
                }
            });
        });
    };
    return ChatSocketHandler;
}());
exports.ChatSocketHandler = ChatSocketHandler;
//# sourceMappingURL=chat.socket.handler.js.map