"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Events;
(function (Events) {
    Events["NEW_MESSAGE"] = "new message";
    Events["ENTER_CHANNEL"] = "enter channel";
    Events["LEAVE_CHANNEL"] = "leave channel";
    Events["INIT_CHAT"] = "init chat";
    Events["NEW_GROUP"] = "new group";
    Events["LIST_GROUPS"] = "list groups";
    Events["ADD_PLAYER"] = "add player";
    Events["PLAYER_READY"] = "player ready";
    Events["LEAVE_GROUP"] = "leave group";
    Events["STATE_UPDATE"] = "state update";
    Events["UNSUBSCRIBE"] = "unsubscribe";
    Events["DRAW"] = "draw";
    Events["GUESS"] = "guess";
    Events["CLUE"] = "clue";
    Events["GAME_FINISHED"] = "game finished";
    Events["START_VIRTUAL"] = "start virtual";
    Events["ROUND_FINISHED"] = "round finished";
    Events["TOURNAMENT_STATE"] = "tournament state";
    Events["TOURNAMENT_FINISHED"] = "tournament finished";
})(Events = exports.Events || (exports.Events = {}));
//# sourceMappingURL=events.js.map