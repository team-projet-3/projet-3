"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chat_server_controller_1 = require("../controllers/chat.server.controller");
var ChatRoute = /** @class */ (function () {
    function ChatRoute(app) {
        app.route("/channel").post(chat_server_controller_1.chatController.createChannel);
        app.route("/channel/join").post(chat_server_controller_1.chatController.joinChannel);
        app.route("/channels").get(chat_server_controller_1.chatController.getChannels);
        app.route("/channel/history/:name").get(chat_server_controller_1.chatController.getChannelHistory);
    }
    return ChatRoute;
}());
exports.default = ChatRoute;
//# sourceMappingURL=chat.server.route.js.map