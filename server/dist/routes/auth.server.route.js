"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var auth_server_controller_1 = require("../controllers/auth.server.controller");
var AuthRoute = /** @class */ (function () {
    function AuthRoute(app) {
        app.route("/auth").post(auth_server_controller_1.authController.auth);
        app.route("/user/profile/:username").post(auth_server_controller_1.authController.updateProfile);
        app.route("/user/profile/:username").get(auth_server_controller_1.authController.getProfile);
        app.route("/tutorial/:username/:tutorial").post(auth_server_controller_1.authController.tutorial);
        app.route("/user/delete/:username").post(auth_server_controller_1.authController.deleteUser);
    }
    return AuthRoute;
}());
exports.default = AuthRoute;
//# sourceMappingURL=auth.server.route.js.map