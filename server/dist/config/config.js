"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var glob_1 = require("glob");
var lodash_1 = require("lodash");
var Config = /** @class */ (function () {
    function Config() {
    }
    Config.globFiles = function (location) {
        return lodash_1.union([], glob_1.sync(location));
    };
    Config.port = process.env.PORT || "5050";
    Config.routes = "./dist/routes/**/*.js";
    Config.models = "./dist/models/**/*.js";
    Config.useMongo = true;
    Config.mongodb = "mongodb+srv://admin:DnjtV35GeUs6H1EK@cluster0-yltcq.gcp.mongodb.net/database?retryWrites=true&w=majority";
    return Config;
}());
exports.default = Config;
//# sourceMappingURL=config.js.map