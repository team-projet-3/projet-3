"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_model_1 = require("../models/user.model");
var bcrypt = require("bcrypt");
var picker_1 = require("./core/picker");
var match_model_1 = require("../models/match.model");
var game_server_service_1 = require("./game.server.service");
var lodash_1 = require("lodash");
var chat_server_service_1 = require("./chat.server.service");
var SALT_ROUNDS = 10;
var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.validate = function (body) {
        return body.username !== undefined && body.password !== undefined;
    };
    AuthService.prototype.login = function (username, password) {
        return __awaiter(this, void 0, void 0, function () {
            var status, user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!username || !password) {
                            return [2 /*return*/, { code: 400, msg: "Invalid parameters", data: null }];
                        }
                        return [4 /*yield*/, this.getUser(username)];
                    case 1:
                        status = _a.sent();
                        if (!status[0]) {
                            return [2 /*return*/, { code: 404, msg: "Username Not Found", data: null }];
                        }
                        user = status[1];
                        console.log(user);
                        return [4 /*yield*/, bcrypt.compare(password, user.password)];
                    case 2:
                        if (!(_a.sent())) {
                            return [2 /*return*/, {
                                    code: 401,
                                    msg: "Username and password don't match",
                                    data: null,
                                }];
                        }
                        if (user.connected) {
                            return [2 /*return*/, {
                                    code: 409,
                                    msg: "User already connected.",
                                    data: null,
                                }];
                        }
                        console.log("User Logged In");
                        user.loginTimes.push(new Date(Date.now()).toLocaleString());
                        user.connected = true;
                        return [4 /*yield*/, user.save()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, { code: 200, msg: "Logged In!", data: picker_1.pick(user_model_1.IUser, user) }];
                }
            });
        });
    };
    AuthService.prototype.register = function (username, password) {
        return __awaiter(this, void 0, void 0, function () {
            var user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, bcrypt.hash(password, SALT_ROUNDS)];
                    case 1:
                        password = _a.sent();
                        user = new user_model_1.User({ username: username, password: password });
                        return [4 /*yield*/, user.save()];
                    case 2:
                        _a.sent();
                        console.log("User Created");
                        return [2 /*return*/, picker_1.pick(user_model_1.IUser, user)];
                }
            });
        });
    };
    AuthService.prototype.getUser = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var user, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, user_model_1.User.findOne({ username: username }).exec()];
                    case 1:
                        user = _a.sent();
                        if (user === null)
                            throw new Error("");
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        return [2 /*return*/, [false, undefined]];
                    case 3: return [2 /*return*/, [true, user]];
                }
            });
        });
    };
    AuthService.prototype.updateProfile = function (username, u) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, status, user, e_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.getUser(username)];
                    case 1:
                        _a = _b.sent(), status = _a[0], user = _a[1];
                        if (!status) {
                            return [2 /*return*/, { code: 404, msg: "Username Not Found", data: null }];
                        }
                        _b.label = 2;
                    case 2:
                        _b.trys.push([2, 4, , 5]);
                        user.firstName = u.firstName ? u.firstName : user.firstName;
                        user.lastName = u.lastName ? u.lastName : user.lastName;
                        user.avatar = u.avatar ? u.avatar : user.avatar;
                        return [4 /*yield*/, user.save()];
                    case 3:
                        _b.sent();
                        return [2 /*return*/, { code: 200, msg: "Profile Updated!", data: null }];
                    case 4:
                        e_2 = _b.sent();
                        return [2 /*return*/, { code: 400, msg: "Malformed request", data: null }];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.getProfile = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, status, user, firstName, lastName, avatar, loginTimes, logoutTimes, matches, classic, solo, coop, tRound, t, matchsPlays, v, vmatchlength, highestSolo, highestCoop, tournamentsWon, history, gameHistory, data;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.getUser(username)];
                    case 1:
                        _a = _b.sent(), status = _a[0], user = _a[1];
                        if (!status)
                            return [2 /*return*/, { code: 404, msg: "Username Not Found", data: null }];
                        firstName = user.firstName, lastName = user.lastName, avatar = user.avatar, loginTimes = user.loginTimes, logoutTimes = user.logoutTimes;
                        return [4 /*yield*/, new game_server_service_1.GameService().getMatches(username)];
                    case 2:
                        matches = _b.sent();
                        classic = matches.filter(function (m) { return m.mode === match_model_1.GameMode.CLASSIC; });
                        solo = matches.filter(function (m) { return m.mode === match_model_1.GameMode.SOLO; });
                        coop = matches.filter(function (m) { return m.mode === match_model_1.GameMode.COOP; });
                        tRound = matches.filter(function (m) { return m.mode === match_model_1.GameMode.TOURNAMENT_ROUND; });
                        t = matches.filter(function (m) { return m.mode === match_model_1.GameMode.TOURNAMENT; });
                        matchsPlays = __spreadArrays(classic, solo, coop, tRound);
                        v = __spreadArrays(classic.filter(function (m) { return m.resultClassic.w.includes(username); }), tRound.filter(function (m) { return m.resultClassic.w.includes(username); }));
                        vmatchlength = classic.length + tRound.length;
                        highestSolo = lodash_1.maxBy(solo, function (m) { return m.resultCoop; });
                        highestCoop = lodash_1.maxBy(coop, function (m) { return m.resultCoop; });
                        tournamentsWon = t.filter(function (m) { return m.resultClassic.w.includes(username); })
                            .length;
                        history = loginTimes.map(function (l, i) {
                            return {
                                login: l || "",
                                logout: logoutTimes[i] || "",
                            };
                        });
                        gameHistory = matches.map(function (m) {
                            return __assign(__assign({}, picker_1.pick(match_model_1.IMatch, m)), { localStart: new Date(m.start).toLocaleString(), localEnd: new Date(m.end).toLocaleString() });
                        });
                        data = {
                            username: username,
                            firstName: firstName,
                            lastName: lastName,
                            avatar: avatar,
                            matchsPlayed: matchsPlays.length,
                            victoryPercent: vmatchlength === 0 ? 0 : v.length / vmatchlength,
                            averageMatchDuration: lodash_1.meanBy(matchsPlays, function (m) { return m.duration; }) || 0,
                            totalMatchDuration: lodash_1.sumBy(matchsPlays, function (m) { return m.duration; }),
                            soloHighestScore: highestSolo ? highestSolo.resultCoop : 0,
                            coopHighestScore: highestCoop ? highestCoop.resultCoop : 0,
                            tournamentsWon: tournamentsWon,
                            history: history,
                            gameHistory: gameHistory,
                        };
                        return [2 /*return*/, { code: 200, msg: "", data: data }];
                }
            });
        });
    };
    AuthService.prototype.logout = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, status, user;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.getUser(username)];
                    case 1:
                        _a = _b.sent(), status = _a[0], user = _a[1];
                        if (!status)
                            return [2 /*return*/];
                        user.logoutTimes.push(new Date(Date.now()).toLocaleString());
                        user.connected = false;
                        user.save();
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.tutorial = function (username, tutorial) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, status, user, tutorials;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.getUser(username)];
                    case 1:
                        _a = _b.sent(), status = _a[0], user = _a[1];
                        if (!status)
                            return [2 /*return*/, { code: 404, msg: "Username Not Found", data: null }];
                        tutorials = __assign({}, user.tutorials);
                        tutorials[tutorial] = false;
                        user.tutorials = tutorials;
                        return [4 /*yield*/, user.save()];
                    case 2:
                        _b.sent();
                        return [2 /*return*/, { code: 200, msg: "Tutorial Done!", data: null }];
                }
            });
        });
    };
    AuthService.prototype.deleteUser = function (username) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, status, user, chat, promises;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.getUser(username)];
                    case 1:
                        _a = _b.sent(), status = _a[0], user = _a[1];
                        if (!status)
                            return [2 /*return*/, { code: 404, msg: "Username Not Found", data: null }];
                        chat = new chat_server_service_1.ChatService();
                        promises = user.channels.map(function (c) { return chat.removeUser(username, c); });
                        return [4 /*yield*/, Promise.all(promises)];
                    case 2:
                        _b.sent();
                        return [4 /*yield*/, user_model_1.User.deleteOne({ username: username }).exec()];
                    case 3:
                        _b.sent();
                        return [2 /*return*/, { code: 200, msg: "User deleted", data: null }];
                }
            });
        });
    };
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=auth.server.service.js.map