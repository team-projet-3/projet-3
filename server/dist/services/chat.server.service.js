"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var channel_model_1 = require("../models/channel.model");
var auth_server_service_1 = require("./auth.server.service");
var util_1 = require("util");
var ChatService = /** @class */ (function () {
    function ChatService() {
        this.authService = new auth_server_service_1.AuthService();
    }
    ChatService.prototype.createChannel = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var channel, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (name === "game")
                            return [2 /*return*/, { code: 409, msg: "Channel Already Exists!", data: null }];
                        channel = new channel_model_1.Channel({ name: name });
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, channel.save()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, { code: 200, msg: "Created!", data: null }];
                    case 3:
                        e_1 = _a.sent();
                        return [2 /*return*/, { code: 409, msg: "Channel Already Exists!", data: null }];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ChatService.prototype.deleteChannel = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (["game", "general"].includes(name))
                            return [2 /*return*/, { code: 409, msg: "Channel Cannot be deleted!", data: null }];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, channel_model_1.Channel.deleteOne({ name: name }).exec()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, { code: 200, msg: "Deleted!", data: null }];
                    case 3:
                        e_2 = _a.sent();
                        return [2 /*return*/, { code: 404, msg: "Channel Doesn't Exists!", data: null }];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ChatService.prototype.joinChannel = function (username, channelName) {
        return __awaiter(this, void 0, void 0, function () {
            var channelNames, status, user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        channelNames = util_1.isArray(channelName)
                            ? channelName
                            : [channelName];
                        console.log(channelNames);
                        return [4 /*yield*/, this.checkChannels(channelNames, username)];
                    case 1:
                        status = _a.sent();
                        console.log(status);
                        if (status[0]) {
                            return [2 /*return*/, { code: 404, msg: status[0] + " Not Found!", data: null }];
                        }
                        user = status[1];
                        channelNames.forEach(function (name) {
                            if (user.channels.indexOf(name) === -1) {
                                user.channels.push(name);
                            }
                        });
                        return [4 /*yield*/, user.save()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, { code: 200, msg: "Channel Joined!", data: null }];
                }
            });
        });
    };
    ChatService.prototype.getChannels = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, channel_model_1.Channel.find().exec()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ChatService.prototype.addMessage = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var channelStatus, chat;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getChannel(message.channel)];
                    case 1:
                        channelStatus = _a.sent();
                        if (!channelStatus[0])
                            return [2 /*return*/, false];
                        message.created = new Date().toLocaleString();
                        chat = channelStatus[1].chat;
                        chat.push(message);
                        channelStatus[1].save();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    ChatService.prototype.getChannel = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var channel, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, channel_model_1.Channel.findOne({ name: name }).exec()];
                    case 1:
                        channel = _a.sent();
                        if (channel === null)
                            throw new Error("");
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        return [2 /*return*/, [false, undefined]];
                    case 3: return [2 /*return*/, [true, channel]];
                }
            });
        });
    };
    ChatService.prototype.removeUser = function (username, ch) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, status, channel;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.getChannel(ch)];
                    case 1:
                        _a = _b.sent(), status = _a[0], channel = _a[1];
                        if (!status)
                            return [2 /*return*/];
                        channel.chat = channel.chat.filter(function (c) { return c.author !== username; });
                        return [4 /*yield*/, channel.save()];
                    case 2:
                        _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ChatService.prototype.checkChannels = function (channels, username) {
        return __awaiter(this, void 0, void 0, function () {
            var channelStatus, userStatus, errorStatus;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Promise.all(channels.map(function (c) { return _this.getChannel(c); }))];
                    case 1:
                        channelStatus = _a.sent();
                        return [4 /*yield*/, this.authService.getUser(username)];
                    case 2:
                        userStatus = _a.sent();
                        console.log(channelStatus);
                        errorStatus = channelStatus.some(function (c) { return !c[0]; })
                            ? "Channel"
                            : !userStatus[0]
                                ? "User"
                                : null;
                        return [2 /*return*/, [errorStatus, userStatus[1]]];
                }
            });
        });
    };
    return ChatService;
}());
exports.ChatService = ChatService;
//# sourceMappingURL=chat.server.service.js.map