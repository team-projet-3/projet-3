"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var IUser = /** @class */ (function () {
    function IUser() {
        this.username = undefined;
        this.password = undefined;
        this.channels = undefined;
        this.created = undefined;
        this.firstName = undefined;
        this.lastName = undefined;
        this.avatar = undefined;
        this.loginTimes = undefined;
        this.logoutTimes = undefined;
        this.connected = undefined;
        this.tutorials = undefined;
    }
    return IUser;
}());
exports.IUser = IUser;
exports.UserSchema = new mongoose_1.Schema({
    username: {
        type: mongoose_1.Schema.Types.String,
        unique: true,
        required: true,
    },
    password: {
        type: mongoose_1.Schema.Types.String,
        required: true,
    },
    channels: {
        type: [String],
        default: ["general", "game"],
    },
    created: {
        type: mongoose_1.Schema.Types.String,
        default: Date.now(),
    },
    firstName: {
        type: mongoose_1.Schema.Types.String,
    },
    lastName: {
        type: mongoose_1.Schema.Types.String,
    },
    avatar: {
        type: mongoose_1.Schema.Types.String,
    },
    loginTimes: {
        type: [String],
        default: [new Date(Date.now()).toLocaleString()],
    },
    logoutTimes: {
        type: [String],
        default: [],
    },
    connected: {
        type: mongoose_1.Schema.Types.Boolean,
        default: true,
    },
    tutorials: {
        type: mongoose_1.Schema.Types.Mixed,
        default: {
            classic: true,
            solo: true,
            coop: true,
            tournament: true,
        },
    },
});
exports.User = mongoose_1.model("User", exports.UserSchema);
//# sourceMappingURL=user.model.js.map