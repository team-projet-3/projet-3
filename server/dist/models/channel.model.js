"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var IChatMessage = /** @class */ (function () {
    function IChatMessage() {
        this.author = undefined;
        this.content = undefined;
        this.channel = undefined;
        this.created = undefined;
    }
    return IChatMessage;
}());
exports.IChatMessage = IChatMessage;
var IChannel = /** @class */ (function () {
    function IChannel() {
        this.name = undefined;
        this.chat = undefined;
    }
    return IChannel;
}());
exports.IChannel = IChannel;
exports.ChatMessageSchema = new mongoose_1.Schema({
    author: {
        type: mongoose_1.Schema.Types.String,
        required: true,
    },
    content: {
        type: mongoose_1.Schema.Types.String,
        required: true,
    },
    type: {
        type: mongoose_1.Schema.Types.String,
        required: true,
    },
    created: {
        type: mongoose_1.Schema.Types.String,
        required: true,
    },
});
exports.ChannelSchema = new mongoose_1.Schema({
    name: {
        type: mongoose_1.Schema.Types.String,
        unique: true,
        required: true,
    },
    chat: {
        type: [Object],
        default: [],
    },
});
exports.Channel = mongoose_1.model("Channel", exports.ChannelSchema);
//# sourceMappingURL=channel.model.js.map