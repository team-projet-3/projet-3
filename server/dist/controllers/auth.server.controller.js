"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var auth_server_service_1 = require("../services/auth.server.service");
var AuthController = /** @class */ (function () {
    function AuthController() {
    }
    AuthController.prototype.auth = function (req, res, _next) {
        return __awaiter(this, void 0, void 0, function () {
            var authService, _a, username, password, status, user;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        authService = new auth_server_service_1.AuthService();
                        if (!authService.validate(req.body)) {
                            res.status(400).json({ msg: "Malformed Request" });
                        }
                        _a = req.body, username = _a.username, password = _a.password;
                        return [4 /*yield*/, authService.login(username, password)];
                    case 1:
                        status = _b.sent();
                        if (!(status.code !== 404)) return [3 /*break*/, 2];
                        res.status(status.code).json({ msg: status.msg, user: status.data });
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, authService.register(username, password)];
                    case 3:
                        user = _b.sent();
                        res.status(200).json({ msg: "Account Created!", user: user });
                        _b.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AuthController.prototype.updateProfile = function (req, res, _next) {
        return __awaiter(this, void 0, void 0, function () {
            var username, _a, firstName, lastName, avatar, service, u, r;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        username = req.params.username;
                        _a = req.body, firstName = _a.firstName, lastName = _a.lastName, avatar = _a.avatar;
                        service = new auth_server_service_1.AuthService();
                        u = { firstName: firstName, lastName: lastName, avatar: avatar };
                        return [4 /*yield*/, service.updateProfile(username, u)];
                    case 1:
                        r = _b.sent();
                        res.status(r.code).json({ msg: r.msg });
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthController.prototype.getProfile = function (req, res, _next) {
        return __awaiter(this, void 0, void 0, function () {
            var username, service, _a, code, msg, data;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        username = req.params.username;
                        service = new auth_server_service_1.AuthService();
                        return [4 /*yield*/, service.getProfile(username)];
                    case 1:
                        _a = _b.sent(), code = _a.code, msg = _a.msg, data = _a.data;
                        res.status(code).json({ msg: msg, data: data });
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthController.prototype.tutorial = function (req, res, _next) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, tutorial, username, service, _b, code, msg;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _a = req.params, tutorial = _a.tutorial, username = _a.username;
                        service = new auth_server_service_1.AuthService();
                        return [4 /*yield*/, service.tutorial(username, tutorial)];
                    case 1:
                        _b = _c.sent(), code = _b.code, msg = _b.msg;
                        res.status(code).json({ msg: msg });
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthController.prototype.deleteUser = function (req, res, _next) {
        return __awaiter(this, void 0, void 0, function () {
            var username, service, _a, code, msg;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        username = req.params.username;
                        service = new auth_server_service_1.AuthService();
                        return [4 /*yield*/, service.deleteUser(username)];
                    case 1:
                        _a = _b.sent(), code = _a.code, msg = _a.msg;
                        res.status(code).json({ msg: msg });
                        return [2 /*return*/];
                }
            });
        });
    };
    return AuthController;
}());
exports.default = AuthController;
exports.authController = new AuthController();
//# sourceMappingURL=auth.server.controller.js.map