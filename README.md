## Quick Links

- [Redmine](https://redmine.gi.polymtl.ca/projects/log3900-06?jump=welcome)
- [Moodle](https://moodle.polymtl.ca/course/view.php?id=985)
- [Requirements](https://docs.google.com/spreadsheets/d/10L4FsfWodPvQHkmUzja3fJy1-ZfjFNsNNXJYFfSjBx0/edit#gid=58266002)
- [Artifacts](https://drive.google.com/drive/folders/1vn4XskFl3zbJs6fnI25zDw1bJveeh81X)

<p align="center">
  <h3 align="center">PROJET 3</h3>
  <p align="center">
    DESCRIPTION
    </p>
</p>

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [About the Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
- [Roadmap](#roadmap)

<!-- ABOUT THE PROJECT -->

## About The Project

### Built With

- []()
- []()
- []()

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

- npm

```sh
npm install npm@latest -g
```

### Installation

1. Clone the repo

```sh
git clone https:://github.com/github_username/repo.git
```

2. Install NPM packages

```sh
npm install
```

<!-- USAGE EXAMPLES -->

## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://example.com)_

<!-- ROADMAP -->

## Roadmap

See the [open issues](https://github.com/github_username/repo/issues) for a list of proposed features (and known issues).
